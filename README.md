# Material Properties

Theoretical material properties according to literatures. The values do not represent real measured properties from experimental works. The properties are:
- Saturation magnetization Ms [kA/m]
- Anisotropy constant Ku [kJ/m3]
- Density [g/cm3]